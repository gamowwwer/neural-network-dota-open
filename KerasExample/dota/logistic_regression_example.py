import pandas
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
import datetime


features = pandas.read_csv('../../resources/Data/features.csv', index_col='match_id')
key_list = list(features.keys())
future_head = list(reversed(list(features.keys())[len(key_list):len(key_list)-7:-1]))
future = features[future_head]

y = future['radiant_win']

X_head = [key for key in key_list if key not in future_head]
X = features[X_head]
X.fillna(0.0, inplace=True)

print(X[:5])
print(y[:5])

seed = 7
np.random.seed(seed)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=seed)

scaler = StandardScaler()
scaler.fit_transform(X_train)


grid = {'C': np.power(10.0, np.arange(-5, 6))}
cv = KFold(n_splits=5, shuffle=True, random_state=241)

start_time = datetime.datetime.now()
lr = LogisticRegression(penalty='l2')

gs = GridSearchCV(lr, grid, scoring='accuracy', cv=cv)
gs.fit(X_train, y_train)

elapsed_time = datetime.datetime.now() - start_time
print("Time elapsed:", elapsed_time)

print(gs.cv_results_)
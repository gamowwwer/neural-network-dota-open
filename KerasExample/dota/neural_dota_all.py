import pandas
import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import datetime



def plot_graphs(history, filename_acc, filename_loss):
    # summarize history for accuracy
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.grid(True)
    plt.savefig(filename_acc)
    #'../../resources/Data/dota_acc.png'
    plt.show()

    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.grid(True)
    plt.savefig(filename_loss)
    #'../../resources/Data/dota_loss.png'
    plt.show()


def model():
    model = Sequential()
    model.add(Dense(120, input_dim=102, activation='relu'))
    model.add(Dense(120, activation="relu"))
    model.add(Dense(2, activation="softmax"))
    model.compile(loss="mean_squared_error", optimizer='adam', metrics=['accuracy'])
    return model


def get_data():
    features = pandas.read_csv('../../resources/Data/features.csv', index_col='match_id')
    key_list = list(features.keys())
    future_head = list(reversed(list(features.keys())[len(key_list):len(key_list) - 7:-1]))
    future = features[future_head]

    y = future['radiant_win']

    X_head = [key for key in key_list if key not in future_head]
    X = features[X_head]
    X.fillna(0.0, inplace=True)

    print(X.shape)
    print(y.shape)

    return X, y


def get_data_less():
    features = pandas.read_csv('../../resources/Data/features.csv', index_col='match_id')
    key_list = list(features.keys())
    future_head = list(reversed(list(features.keys())[len(key_list):len(key_list) - 7:-1]))

    hero_head = ['lobby_type', 'r1_hero', 'r2_hero', 'r2_hero', 'r3_hero', 'r4_hero', 'r5_hero', 'd1_hero', 'd2_hero',
                 'd2_hero', 'd3_hero', 'd4_hero', 'd5_hero']

    future = features[future_head]

    y = future['radiant_win']

    X_head = [key for key in key_list if key not in future_head + hero_head]
    X = features[X_head]
    X.fillna(0.0, inplace=True)

    print(X.shape)
    print(y.shape)

    return X, y


def get_data_test():
    features = pandas.read_csv('../../resources/Data/features_test.csv', index_col='match_id')
    key_list = list(features.keys())
    future_head = list(reversed(list(features.keys())))
    future = features[future_head]

    X_head = [key for key in key_list if key not in future_head]
    X = features[X_head]
    X.fillna(0.0, inplace=True)

    print(X.shape)

    return X


def train_model_raw():
    X, y = get_data()

    encoder = LabelEncoder()
    encoder.fit(y)
    encoded_y = encoder.transform(y)
    dummy_y = np_utils.to_categorical(encoded_y)

    print("learning")
    estimator = KerasClassifier(build_fn=model, epochs=50, batch_size=5, verbose=1)

    start_time = datetime.datetime.now()

    history = estimator.fit(X, dummy_y, validation_split=0.25)

    print("acc: %.3f; loss: %.3f" % (
        history.history['acc'][len(history.history['acc']) - 1],
        history.history['loss'][len(history.history['loss']) - 1]))

    elapsed_time = datetime.datetime.now() - start_time
    print(elapsed_time)

    with open('../../resources/dota/dota.txt', 'w') as f:
        f.write("Raw data model:\n")
        f.write("acc: %.3f; loss: %.3f" % (history.history['acc'][len(history.history['acc']) - 1],
                                           history.history['loss'][len(history.history['loss']) - 1]))

    plot_graphs(history, '../../resources/dota/dota_acc_raw.png',
                '../../resources/dota/dota_loss_raw.png')


def train_model_less():
    X, y = get_data_less()

    scaler = StandardScaler()
    X = scaler.fit_transform(X)

    encoder = LabelEncoder()
    encoder.fit(y)
    encoded_y = encoder.transform(y)
    dummy_y = np_utils.to_categorical(encoded_y)

    print("learning")
    estimator = KerasClassifier(build_fn=model, epochs=50, batch_size=5, verbose=1)

    start_time = datetime.datetime.now()

    history = estimator.fit(X, dummy_y, validation_split=0.25)

    print("acc: %.3f; loss: %.3f" % (
        history.history['acc'][len(history.history['acc']) - 1],
        history.history['loss'][len(history.history['loss']) - 1]))

    elapsed_time = datetime.datetime.now() - start_time
    print(elapsed_time)

    with open('../../resources/dota/dota.txt', 'a') as f:
        f.write('Less feature model:\n')
        f.write("acc: %.3f; loss: %.3f" % (history.history['acc'][len(history.history['acc']) - 1],
                                           history.history['loss'][len(history.history['loss']) - 1]))

    plot_graphs(history, '../../resources/dota/dota_acc_less.png',
                '../../resources/dota/dota_loss_less.png')


def train_model():
    X, y = get_data()

    scaler = StandardScaler()
    X = scaler.fit_transform(X)

    encoder = LabelEncoder()
    encoder.fit(y)
    encoded_y = encoder.transform(y)
    dummy_y = np_utils.to_categorical(encoded_y)

    print("learning")
    estimator = KerasClassifier(build_fn=model, epochs=50, batch_size=5, verbose=1)

    start_time = datetime.datetime.now()

    history = estimator.fit(X, dummy_y, validation_split=0.25)

    print("acc: %.3f; loss: %.3f" % (
        history.history['acc'][len(history.history['acc']) - 1],
        history.history['loss'][len(history.history['loss']) - 1]))

    elapsed_time = datetime.datetime.now() - start_time
    print(elapsed_time)

    with open('../../resources/dota/dota.txt', 'a') as f:
        f.write("Clean data model:\n")
        f.write("acc: %.3f; loss: %.3f" % (history.history['acc'][len(history.history['acc']) - 1],
                                           history.history['loss'][len(history.history['loss']) - 1]))

    plot_graphs(history, '../../resources/dota/dota_acc.png',
                '../../resources/dota/dota_loss.png')
    return estimator, scaler


def predict_test(estimator, scaler):
    X_test = get_data_test()
    X_test = scaler.transform(X_test)

    #y_test = estimator.predict(X_test)
    y_test = estimator.predict_proba(X_test)

    print(y_test.shape)

    with open('../../resources/dota/dota.txt', 'a') as f:
        f.write("Predict probability:\n")


def __main__():
    #train_model_raw()
    #train_model_less()
    est, sc = train_model()
    predict_test(est, sc)



__main__()
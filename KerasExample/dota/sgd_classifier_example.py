import pandas
import numpy as np
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split


features = pandas.read_csv('../../resources/Data/features.csv', index_col='match_id')
key_list = list(features.keys())
future_head = list(reversed(list(features.keys())[len(key_list):len(key_list)-7:-1]))
future = features[future_head]

y = future['radiant_win']

X_head = [key for key in key_list if key not in future_head]
X = features[X_head]
X.fillna(0.0, inplace=True)

seed = 7
np.random.seed(seed)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=seed)

scaler = StandardScaler()
scaler.fit_transform(X_train)

print(X.shape)
print(y.shape)

clf = SGDClassifier(loss='squared_loss', penalty='l2')
clf.fit(X_train, y_train)

X_test = scaler.transform(X_test)

score = clf.score(X_test, y_test)

print(score)
